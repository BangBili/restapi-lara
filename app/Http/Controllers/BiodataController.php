<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Biodata;
use Illuminate\Support\Facades\Auth;

class BiodataController extends Controller
{
    protected $biodata = Biodata::class;
   
    public function index()
    {
        $biodata = Biodata::all();
        return response()->json($biodata);

    }

    public function store(Request $request)
    {
        Biodata::create($request->all());
        return 'Data berhasil Ditambahkan';
    }

  
    public function show($id)
    {
        $biodata = Biodata::find($id);
        return response()->json($biodata);
    }


    public function update(Request $request, $id)
    {
        $biodata = Biodata::find($id);
        $biodata->update($request->all());
        return $biodata->toJson();
    }

  
    public function delete($id)
    {
        $biodata = Biodata::find($id);
        $biodata->delete($id);
        return 'data dengan id '.$id.' berhasil dihapus dihapus';
    }

    public function resolveUser()
    {
        return Auth::guard('custom-guard')->user();
    }
}
